# .NET

Vous devez developper une programme qui permet de gerer une bibliotheque de livres ou une boutique de livres en ligne.

Vous utiliserez l'une des technologies suivantes fournies par •net core, a savoir:

- ASP NET Core Web API
- ASP .NET Core MVC
- ASP NET Core Razor Pages
- Blazor
- ASP .NET Core Web API + Client Side (Angular, React, Vue. js, ...)
- MAUI (Multi-platform App UI)

Les fonctionnalites suivantes doivent etre implementees, elles sont calques sur le programme du cours de la semaine :

- Modele avec EF Core
- Architecture MVC
- Au moins un CRUD (Create, Read, Update, Delete)
- Gestion d'erreur
- Routage
- Créer des controllers
- Créer des vues (optionnel)
- Ecrire un test unitaire (optionnel)
- Persistance des donnes avec EF Core SQLite

- Implémenter les bonnes pratiques et les fonctionnalités .NET Cire vues en cours
