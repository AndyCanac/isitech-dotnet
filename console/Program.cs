﻿namespace console;

class Program
{
static void Main(string[] args)
    {
        Console.WriteLine("Hello, world");
        String? s = null;
        Console.WriteLine(s);
        int[] array = new int[] {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
        var maRange = array[0..^2];
        Console.WriteLine(string.Join(", ", maRange));
    }
}

public class Eleve(){

    public string Name { get; set; }

    public int Age { get; set; }

    public string Prenom { get; set; }

}

public class Professeur(){

    public string Name { get; set; }

    public int Age { get; set; }

    public string Prenom { get; set; }

}
