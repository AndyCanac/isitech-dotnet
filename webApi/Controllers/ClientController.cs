using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using webApi.models;
using AutoMapperDemo;

namespace webApi.Controllers;

[ApiController]
[Route("api/[controller]")]

public class ClientController : ControllerBase
{
    private readonly AppDbContext _context;

    private readonly IMapper _mapper;

    public ClientController(AppDbContext context)
    {
        _context = context;
        _mapper = MapperConfig.InitializeAutoMapper();
    }

    [HttpGet]
    public async Task<IEnumerable<Clients>> Get()
    {
        return await _context.Clients.ToListAsync();
    }
}