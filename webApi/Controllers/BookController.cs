using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using webApi.models;
using webApi.models.DTOs;
using AutoMapperDemo;

namespace webApi.Controllers;

[ApiController]
[Route("api/[controller]")]

public class BooksController : ControllerBase
{
    private readonly AppDbContext _context;

    private readonly IMapper _mapper;

    public BooksController(AppDbContext context)
    {
        _context = context;
        _mapper = MapperConfig.InitializeAutoMapper();
    }

    [HttpGet]

    public async Task<IEnumerable<Books>> Get()
    {
        return await _context.Books.ToListAsync();
    }

    // GET: api/Book/[id]
    [HttpGet("{id}", Name = nameof(GetBook))]
    public async Task<ActionResult<Books>> GetBook(int id)
    {
        var book = await _context.Books.FindAsync(id);
        if (book == null)
        {
            return NotFound();
        }

        return book;
    }

    // POST: api/Book
    // BODY: Book (JSON)
    [HttpPost]
    [ProducesResponseType(201, Type = typeof(Books))]
    [ProducesResponseType(400)]
    public async Task<ActionResult<Books>> PostBook([FromBody] Books book)
    {
        // we check if the parameter is null
        if (book == null)
        {
            return BadRequest();
        }
        // we check if the book already exists
        Books? addedBook = await _context.Books.FirstOrDefaultAsync(b => b.Title == book.Title);
        if (addedBook != null)
        {
            return BadRequest("Book already exists");
        }
        else
        {
            // we add the book to the database
            await _context.Books.AddAsync(book);
            await _context.SaveChangesAsync();

            // we return the book
            return CreatedAtRoute(
                routeName: nameof(GetBook),
                routeValues: new { id = book.Id },
                value: book);

        }
    }

    // [HttpPut("put/{id}, Title, Author, Genre, Price, PublishDate, Description, Remarks")]
    // public async Task<ActionResult<Books>> PutBook(int id, string title, string author, string genre, decimal price, DateTime publishDate, string description, string remarks)
    // {
    //     var book = await _context.Books.FindAsync(id);
    //     if (book == null)
    //     {
    //         return NotFound();
    //     }

    //     Books newbooks = book;
    //     if (title != null) newbooks.Title = title;
    //     if (author != null) newbooks.Author = author;
    //     if (genre != null) newbooks.Genre = genre;
    //     if (price != null) newbooks.Price = price;
    //     if (publishDate != null) newbooks.PublishDate = publishDate;
    //     if (description != null) newbooks.Description = description;
    //     if (remarks != null) newbooks.Remarks = remarks;

    //     await _context.SaveChangesAsync();

    //     return newbooks;
    // }

    [HttpPut("{id}")]
    public async Task<ActionResult<BookUpdateDTO>> PutBook(int id, BookUpdateDTO book)
    {
        var selectedBook = await _context.Books.FindAsync(id);

        if (selectedBook == null)
        {
            return NotFound();
        }

        selectedBook.Title = book.Title;
        selectedBook.Author = book.Author;
        selectedBook.PublishDate = book.PublishDate;
        selectedBook.Genre = book.Genre;
        selectedBook.Price = book.Price;
        selectedBook.PublishDate = book.PublishDate;
        selectedBook.Description = book.Description;
        selectedBook.Remarks = book.Remarks;

        _context.Entry(selectedBook).State = EntityState.Modified;
        await _context.SaveChangesAsync();
        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteBook(int id)
    {
        // on vérifie si le livre existe
        var book = await _context.Books.FindAsync(id);
        if (book == null)
        {
            return NotFound();
        }
        _context.Books.Remove(book);
        await _context.SaveChangesAsync();

        return NoContent();
    }

}