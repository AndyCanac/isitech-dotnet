namespace webApi.models;

public class Clients
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Lastname { get; set; }
    public string? Identifiant { get; set; }
    public string? Password { get; set; }
    public string? Key { get; set; }

}