using AutoMapper;
using webApi.models;
using webApi.models.DTOs;
namespace AutoMapperDemo
{
    public class MapperConfig
    {
        public static Mapper InitializeAutoMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BookUpdateDTO, Books>();

            });

            var mapper = new Mapper(config);
            return mapper;
        }
    }
}
