using System.ComponentModel.DataAnnotations;

namespace webApi.models.DTOs

{

    public class BookUpdateDTO
    {

        [MaxLength(30)]
        public string? Title { get; set; }
        [MaxLength(30)]
        public string? Author { get; set; }
        [MaxLength(20)]
        public string? Genre { get; set; }
        public decimal Price { get; set; }
        public DateTime PublishDate { get; set; }
        [MaxLength(300)]
        public string? Description { get; set; }
        [MaxLength(200)]
        public string? Remarks { get; set; }
    }
}